<img width="100" src="https://www.dtapp.net/assets/img/logo.png" alt="https://www.dtapp.net/"/>

<h1><a href="https://www.dtapp.net/">Golang Redis</a></h1>

📦 Golang Redis 缓存数据库

[comment]: <> (dtapps)
[![GitHub Org's stars](https://img.shields.io/github/stars/dtapps)](https://github.com/dtapps)

[comment]: <> (go)
[![godoc](https://pkg.go.dev/badge/github.com/dtapps/go-redis?status.svg)](https://pkg.go.dev/github.com/dtapps/go-redis)
[![goproxy.cn](https://goproxy.cn/stats/github.com/dtapps/go-redis/badges/download-count.svg)](https://goproxy.cn/stats/github.com/dtapps/go-redis)
[![goreportcard.com](https://goreportcard.com/badge/github.com/dtapps/go-redis)](https://goreportcard.com/report/github.com/dtapps/go-redis)
[![deps.dev](https://img.shields.io/badge/deps-go-red.svg)](https://deps.dev/go/github.com%2Fdtapps%2Fgo-redis)

[comment]: <> (github.com)
[![watchers](https://badgen.net/github/watchers/dtapps/go-redis)](https://github.com/dtapps/go-redis/watchers)
[![stars](https://badgen.net/github/stars/dtapps/go-redis)](https://github.com/dtapps/go-redis/stargazers)
[![forks](https://badgen.net/github/forks/dtapps/go-redis)](https://github.com/dtapps/go-redis/network/members)
[![issues](https://badgen.net/github/issues/dtapps/go-redis)](https://github.com/dtapps/go-redis/issues)
[![branches](https://badgen.net/github/branches/dtapps/go-redis)](https://github.com/dtapps/go-redis/branches)
[![releases](https://badgen.net/github/releases/dtapps/go-redis)](https://github.com/dtapps/go-redis/releases)
[![tags](https://badgen.net/github/tags/dtapps/go-redis)](https://github.com/dtapps/go-redis/tags)
[![license](https://badgen.net/github/license/dtapps/go-redis)](https://github.com/dtapps/go-redis/blob/master/LICENSE)
[![GitHub go.mod Go version (subdirectory of monorepo)](https://img.shields.io/github/go-mod/go-version/dtapps/go-redis)](https://github.com/dtapps/go-redis)
[![GitHub release (latest SemVer)](https://img.shields.io/github/v/release/dtapps/go-redis)](https://github.com/dtapps/go-redis/releases)
[![GitHub tag (latest SemVer)](https://img.shields.io/github/v/tag/dtapps/go-redis)](https://github.com/dtapps/go-redis/tags)
[![GitHub pull requests](https://img.shields.io/github/issues-pr/dtapps/go-redis)](https://github.com/dtapps/go-redis/pulls)
[![GitHub issues](https://img.shields.io/github/issues/dtapps/go-redis)](https://github.com/dtapps/go-redis/issues)
[![GitHub code size in bytes](https://img.shields.io/github/languages/code-size/dtapps/go-redis)](https://github.com/dtapps/go-redis)
[![GitHub language count](https://img.shields.io/github/languages/count/dtapps/go-redis)](https://github.com/dtapps/go-redis)
[![GitHub search hit counter](https://img.shields.io/github/search/dtapps/go-redis/go)](https://github.com/dtapps/go-redis)
[![GitHub top language](https://img.shields.io/github/languages/top/dtapps/go-redis)](https://github.com/dtapps/go-redis)

## 安装

```go
go get -u github.com/dtapps/go-redis
```